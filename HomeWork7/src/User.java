public class User {
    String firstName;
    String lastName;
    int age;
    boolean isWorker;

    public static User builder()
    {
        User user = new User();
        return user;
    }

    public User firstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public User lastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public User age(int age) {
        this.age = age;
        return this;
    }

    public User isWorker(boolean worker) {
        this.isWorker = worker;
        return this;
    }
}
