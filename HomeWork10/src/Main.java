public class Main {

    public static void main(String[] args) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(1);
        arrayList.add(2);
        arrayList.add(3);
        arrayList.add(4);
        arrayList.add(5);
        arrayList.add(6);
        arrayList.add(7);
        arrayList.add(8);
        arrayList.add(9);
        arrayList.add(10);
        arrayList.add(11);

        arrayList.removeByIndex(0);
        System.out.println(arrayList.toString());
        arrayList.removeByIndex(100);
        System.out.println(arrayList.toString());
        System.out.println(arrayList.contains(14));
        System.out.println(arrayList.contains(10));
        arrayList.remove(10);
        System.out.println(arrayList.toString());
        arrayList.remove(1000);
        System.out.println(arrayList.toString());


        LinkedList linkedList = new LinkedList();
        linkedList.add(10);
        linkedList.add(20);
        linkedList.add(30);
        linkedList.add(40);
        linkedList.add(50);
        linkedList.add(60);
        linkedList.add(70);

        System.out.println(linkedList.contains(30));
        linkedList.remove(30);
        linkedList.ShowLinkedList();
        linkedList.remove(1000);
        linkedList.ShowLinkedList();
        linkedList.removeByIndex(2);
        linkedList.ShowLinkedList();
        linkedList.removeByIndex(1000);
        linkedList.ShowLinkedList();
        linkedList.removeByIndex(4);
        linkedList.ShowLinkedList();
        linkedList.removeByIndex(0);
        linkedList.ShowLinkedList();
        int x = 0;

    }
}
