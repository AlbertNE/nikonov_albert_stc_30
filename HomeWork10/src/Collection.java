public interface Collection {

    /**
     * Добавляет элемент в коллекцию
     * @param element добавляемый элемент
     */
    void add(int element);

    /**
     * Проверяет, есть ли элемент в коллекции
     * @param element искомый элемент
     * @return true, если элемент присутствует хотя бы один раз в коллекции
     */
    boolean contains(int element);

    /**
     * Получаем количество элементов в коллекции
     * @return возвращает число элементов
     */
    int size();

    /**
     * Удаляет первое вхождение элемента в список
     * @param element удаляемый элемент
     */
    void remove(int element);

}
