public class LinkedList implements List{

    private static class Node {
        int value;
        Node next;

        @Override
        public String toString() {
            return "Node{" +
                    "value=" + value +
                    '}';
        }

        public Node(int value) {
            this.value = value;
        }
    }
    // ссылка на начало списка, на первый узел
    private Node first;
    // ссылка на последний элемент списка, на последний узел
    private Node last;

    private int size;

    @Override
    public int get(int index) {
        if (index >= 0 && index < size) {
            Node current = first;

            for (int i = 0; i < index; i++) {
                // отсчитываете index-штук узлов до нужного
                current = current.next;
            }
            // возвращаете его значение
            return current.value;
        }
        System.err.println("Index out of bounds");
        return -1;
    }

    public void addFirst(int element) {
        Node newNode = new Node(element);
        if (first == null) {
            add(element);
        } else {
            // если уже есть элементы в списке
            // следующий узел после нового - это первый узел списка
            newNode.next = first;
            // теперь новый узел - первый в списке
            first = newNode;
            size++;
        }
    }

    @Override
    public void add(int element) {
        Node newNode = new Node(element);
        // если в списке еще нет элементов
        if (first == null) {
            // то первый и последний элемент - это новый
            first = newNode;
            last = newNode;
        } else {
            // теперь последний элемент списка ссылается на новый, следовательно новый встал в конец списка
            last.next = newNode;
            // теперь новый узел является последним
            last = newNode;
        }
        size++;
    }

    @Override
    public boolean contains(int element) {
      boolean result = false;
       Node currentNode = first;
       while(currentNode.next != null){
           if(currentNode.value == element){
               result = true;
           }
           currentNode = currentNode.next;
       }
       return result;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public void remove(int element) {

        boolean deleted = false;
        Node prev = null;
        Node current = first;

        for (int i = 0; i < size; i++) {
            // отсчитываете index-штук узлов до нужного
            if(current.value == element){
                removeNode(prev,current);
                deleted = true;
                return;
            }
            prev = current;
            current = current.next;
        }

        if(!deleted){
            System.err.println("There is no node with such an element");
        }

    }

    public void removeByIndex(int index)
    {
        if (index >= 0 && index < size) {
            Node prev = null;
            Node current = first;

            for (int i = 0; i < index; i++) {
                // отсчитываете index-штук узлов до нужного
                prev = current;
                current = current.next;
            }
            removeNode(prev,current);
        }else {
            System.err.println("Index out of bounds");
        }
    }

    private void removeNode(Node prev, Node node){
        if(node == first){
            first = node.next;
            node.next = null;
        }else if(node == first){
            last = prev;
            prev.next = null;
        }else {
            prev.next = node.next;
            node.next = null;
        }
        size--;
    }
    public void ShowLinkedList()
    {
        Node current = first;
        for (int i = 0; i < size; i++) {
            System.out.print(String.valueOf(i + 1) + current + " ");
            current = current.next;
        }
        System.out.println();
    }

}
