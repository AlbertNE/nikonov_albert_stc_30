public interface List extends Collection{

    /**
     * Возвращает элемент под заданным индексом
     * @param index индекс элемента
     * @return элемент
     */
    int get(int index);


}
