import java.util.Arrays;

public class ArrayList implements Collection{
    private int[] elements;
    private final int ARRAY_LENGHT = 10;
    private int currentElementsNumber;


    public ArrayList() {
        this.elements = new int[ARRAY_LENGHT];
    }
    public ArrayList(int lenght) {
        this.elements = new int[lenght];
    }

    @Override
    public void add(int element) {
        if(currentElementsNumber == elements.length){
            resize();
        }
        elements[currentElementsNumber] = element;
        currentElementsNumber++;
    }

    @Override
    public int size() {
        return currentElementsNumber;
    }

    @Override
    public boolean contains(int element) {
       boolean result = false;
        for (int i = 0; i < elements.length; i++) {
            if(element == elements[i]) {
                result = true;
            }
        }
        return result;
    }

    @Override
    public void remove(int element) {

        if(!contains(element)){
            System.out.println("Массив не содержит подобного значения");
            return;
        }

        for (int i = 0; i < elements.length; i++) {
            if(element == elements[i]) {
                removeByIndex(i);
            }
        }
    }

    public void removeByIndex(int index) {

        if(index < 0 || index > currentElementsNumber){
            System.out.println("Индекс находится за границей массива");
            return;
        }

        if(index == currentElementsNumber)
        {
            elements[index] = 0;
        }
        System.arraycopy(elements,index + 1,elements,index,currentElementsNumber - index + 1);
        currentElementsNumber--;
    }

    private void resize() {
        this.elements = Arrays.copyOf(elements,elements.length + 10 );
    }

    @Override
    public String toString() {
        return "ArrayList{" +
                "elements=" + Arrays.toString(elements) +
                '}';
    }
}
