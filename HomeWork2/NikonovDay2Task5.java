import java.util.Arrays;
import java.util.Scanner;

public class NikonovDay2Task5{

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите размерность массива: ");
        int x = scanner.nextInt();
        int y = scanner.nextInt();
        int [][] intarray = new int[x][y];
        ArrayData arrayData = new ArrayData(x,y);
        int f = arrayData.x;
        int counter = 0;
        while(counter<y-1-counter)
        {
            fillTheLineForward(intarray,counter,arrayData);
            fillTheColumnForward(intarray,counter,arrayData);
            fillTheLineReverse(intarray,counter,arrayData);
            fillTheColumnReverse(intarray,counter,arrayData);
            counter++;
        }

        for(int i = 0;i < x;i++ )
        {
            System.out.println(Arrays.toString(intarray[i]));
        }

    }
    public static void fillTheColumnReverse(int[][] someArray, int counter, ArrayData arrayData)
    {
        for (int i = arrayData.x-1- counter; i > counter; i--) {
            someArray[i][counter] = arrayData.currentNumber;
            arrayData.increaseTheNuber();
        }
    }
    public static void fillTheLineReverse(int[][] someArray,int counter,ArrayData arrayData)
    {
        for (int i = arrayData.y-1-counter; i > counter; i--) {
            someArray[arrayData.x-1-counter][i] = arrayData.currentNumber;
            arrayData.increaseTheNuber();
        }
    }
    public static void fillTheLineForward(int[][] someArray,int counter,ArrayData arrayData)
    {
        for (int i = 0 + counter; i < arrayData.y-1-counter; i++) {
            someArray[counter][i] = arrayData.currentNumber;
            arrayData.increaseTheNuber();
        }
    }
    public static void fillTheColumnForward(int[][] someArray,int counter,ArrayData arrayData)
    {

        for (int i = 0 + counter; i < arrayData.x-1-counter; i++) {
            someArray[i][arrayData.y - 1 - counter] = arrayData.currentNumber;
            arrayData.increaseTheNuber();
        }
    }
    public static class ArrayData
    {
        private int x;
        private int y;
        private int currentNumber;

        public ArrayData(int x, int y) {
            this.x = x;
            this.y = y;
            currentNumber = 1;
        }

        public void increaseTheNuber() {
            this.currentNumber++;
        }
    }


}
