import java.util.Arrays;

public class NikonovDay2Task3{

    public static void main(String[] args) {
	    int[] intArray = {1,4,6,8,1};
        System.out.println(arrayToInt(intArray));
    }

public static int arrayToInt(int[] x) {
        int number = 0;
        double degree = 0;
        for(int i = x.length - 1;i >=0;i--)
        {
            number = number + x[i]*(int)Math.pow(10.0,degree);
            degree+=calcIntDex(x[i]);
        }
        return number;
    }
    public static int calcIntDex(int x ) {
        int intDex = 0;
        int f = 1;
        while(f >= 1)
        {
            intDex++;
            f = x/(int)Math.pow(10.0,intDex);
        }
        return intDex;
    }
}