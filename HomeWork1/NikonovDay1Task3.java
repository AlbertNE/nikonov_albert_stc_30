import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Program3 {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите числа последовательности через пробел:");
//        ArrayList<Integer> IntList = new ArrayList<Integer>();
//        int number = 1;
//        while(number!=0)
//        {
//            number = scanner.nextInt();
//            if(number!=0) {
//                IntList.add(number);
//            }
//        }
        String input = scanner.nextLine();
        String [] s = input.split(" ");
        int[] intArray = new int[s.length];

        for(int i = 0;i < s.length; i++)
        {
            intArray[i] = Integer.valueOf(s[i]);
        }

        int simpleIntComposition = 1;
        for(int i = 0;i < intArray.length;i++)
        {
            int x = intArray[i];
            int y = sumOfNumbers(x);
            if(isSimple(y)) {
                simpleIntComposition *= x;
            }
        }
        System.out.println(simpleIntComposition);

    }
    static boolean isSimple(int number)
    {
        if(number<2)
        {
            return false;
        }
        for(int i = 2; i <number; i++)
        {
            if(number%i == 0)
            {
               return false;
            }
        }
        return true;
    }
    static int sumOfNumbers(int number)
    {
        int sum = 0;
        int x = sum;
        double i = 0;
        while (number/Math.pow(10,i) > 0)
        {
            sum = sum + (int)(number/Math.pow(10,i))%10;
            i++;
        }

        return sum;
    }
}

