import java.util.Scanner;

public class NikonovDay4{

    public static boolean isDegreeOf2(int x) {
        if (x == 1) {
            return true;
        }
        if (x % 2 != 0) {
            return false;
        }
        return isDegreeOf2(x / 2);
    }

    public static int binarySearch(int[] currentArray, int searchable) {
        int mid = currentArray.length / 2;
        int left = 0;
        int right = currentArray.length - 1;

        if (currentArray[mid] == searchable) {
            return 100;
        }
        while (left < right) {
            if (currentArray[mid] < searchable) {
                left = mid + 1;
            }
            if (currentArray[mid] > searchable) {
                right = mid - 1;
            }
            int[] f = new int[right - left + 1];
            int k = left;
            for (int i = 0; i < f.length; i++) {
                f[i] = currentArray[k];
                k++;
            }
            return binarySearch(f, searchable);
        }
        return -1;
    }

    public static int[] fibanachi(int[] fibArray,int currentIntdex) {
        if(currentIntdex == 0){
            fibArray[currentIntdex] = 0;
        }
        else if(currentIntdex == 1){
            fibArray[currentIntdex] = 1;
        }
        else{
            fibArray[currentIntdex] = fibArray[currentIntdex - 1] + fibArray[currentIntdex - 2];
        }
        currentIntdex++;
        if(currentIntdex < fibArray.length) {
            fibanachi(fibArray, currentIntdex);
        }
        return fibArray;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите число:");
        int x = scanner.nextInt();
        System.out.println(isDegreeOf2(x));
        int[] array = {1, 2, 3, 4, 5, 6, 7, 8};
        int intIndex = binarySearch(array, x);
        if(intIndex == 100) {
            System.out.println("Искомое значение присутствует в текущем массиве");
        }
        else {
            System.out.println("Искомое значение отсутствует в массиве");
        }
        int[] fibArray = new int[x];
        fibanachi(fibArray,0);
        System.out.println(fibArray[x - 1]);
    }
}
