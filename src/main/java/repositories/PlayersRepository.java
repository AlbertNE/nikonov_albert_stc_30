package repositories;

import models.Player;

import java.util.Optional;

public interface PlayersRepository {

    Optional<Player> findByNickname(String nickname);

    void save(Player player);

    void update(Player player);

}
