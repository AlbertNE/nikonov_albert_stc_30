package repositories;

import models.Game;
import models.Player;

import javax.sql.DataSource;
import java.sql.*;
import java.time.LocalDateTime;

public class GamesRepositoryJDBC implements GamesRepository {

    //language=SQL
    private static final String SQL_INSERT = "insert into game(date, first_player_id, second_player_id) values (?, ?, ?)";

    //language=SQL
    private static final String SQL_SELECT_BY_ID = "select * from game where id = ?";

    //language=SQL
    private static final String SQL_UPDATE_BY_ID = "update game set date = ?,first_player_id = ?,second_player_id = ?," +
            "first_player_shots = ?, second_player_shots = ?, game_duration = ? where id = ?";

    DataSource dataSource;

    public GamesRepositoryJDBC(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void save(Game game) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_INSERT, Statement.RETURN_GENERATED_KEYS)) {
            statement.setString(1, game.getDate().toString());
            statement.setLong(2, game.getFirstPlayer().getId());
            statement.setLong(3, game.getSecondPlayer().getId());

            int affectedRows = statement.executeUpdate();

            if (affectedRows != 1) {
                throw new SQLException("Can't insert game");
            }

            ResultSet generatedKeys = statement.getGeneratedKeys();

            if (generatedKeys.next()) {
                game.setId(generatedKeys.getLong("id"));
            } else {
                throw new SQLException("Can't obtain id");
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }

    }

    @Override
    public Game getById(Long gameId){

        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_SELECT_BY_ID)) {
            statement.setLong(1, gameId);
            try (ResultSet result = statement.executeQuery()){
                if (result.next()) {
                    return Game.builder()
                            .id(result.getLong("id"))
                            .date(LocalDateTime.parse(result.getString("date")))
                            .firstPlayer(Player.builder().id(result.getLong("first_player_id")).build())
                            .secondPlayer(Player.builder().id(result.getLong("second_player_id")).build())
                            .build();

                } else {
                    throw new  SQLException("Empty result");
                }
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }

    }

    @Override
    public void update(Game game) {

        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_BY_ID)) {
            statement.setString(1, game.getDate().toString());
            statement.setLong(2, game.getFirstPlayer().getId());
            statement.setLong(3, game.getSecondPlayer().getId());
            statement.setInt(4, game.getFirstPlayerShotsCount());
            statement.setInt(5, game.getSecondPlayerShotsCount());
            statement.setLong(6,game.getGameDuration());
            statement.setLong(7, game.getId());

            int affectedRows = statement.executeUpdate();

            if (affectedRows != 1) {
                throw new SQLException("Can't update game");
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }


    }


}
