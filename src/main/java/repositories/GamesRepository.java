package repositories;

import models.Game;

public interface GamesRepository {
    void save(Game game);

    Game getById(Long gameId);

    void update(Game game);

}
