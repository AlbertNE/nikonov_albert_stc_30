package repositories;

import models.Player;

import javax.sql.DataSource;
import java.sql.*;
import java.util.Optional;

public class PlayersRepositoryJDBC implements PlayersRepository {
    //language=SQL
    private static String SQL_FIND_BY_NICKNAME = "select id, ip, name, max_points_count from player where name = ?";

    //language=SQL
    private static String SQL_INSERT = "insert into player(name, ip) values (?, ?)";

    //language=SQL
    private static String SQL_UPDATE = "update player set ip = ?, name = ?, max_points_count = ?, wins = ?, loses = ? where id = ?";

    private DataSource dataSource;

    public PlayersRepositoryJDBC(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public Optional<Player> findByNickname(String nickname) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_FIND_BY_NICKNAME)) {
            statement.setString(1, nickname);
            try (ResultSet result = statement.executeQuery()){
                if (result.next()) {
                    return Optional.of(Player.builder()
                            .id(result.getLong("id"))
                            .name(result.getString("name"))
                            .ip(result.getString("ip"))
                            .maxPointsCount(result.getInt("points"))
                            .build());
                } else {
                    return Optional.empty();
                }
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public void save(Player player) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_INSERT, Statement.RETURN_GENERATED_KEYS)) {
            statement.setString(1, player.getName());
            statement.setString(2, player.getIp());

            int affectedRows = statement.executeUpdate();

            if (affectedRows != 1) {
                throw new SQLException("Can't insert player");
            }

            ResultSet generatedKeys = statement.getGeneratedKeys();

            if (generatedKeys.next()) {
                player.setId(generatedKeys.getLong("id"));
            } else {
                throw new SQLException("Can't obtain id");
            }

        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public void update(Player player) {

        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_UPDATE)) {
            statement.setString(1, player.getIp());
            statement.setString(2, player.getName());
            statement.setInt(3, player.getMaxPointsCount());
            statement.setInt(4, player.getWins());
            statement.setInt(5, player.getLoses());
            statement.setLong(6, player.getId());

            int affectedRows = statement.executeUpdate();

            if (affectedRows != 1) {
                throw new SQLException("Can't update player");
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }

    }

}
