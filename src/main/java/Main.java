import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import com.zaxxer.hikari.util.DriverDataSource;
import models.Game;
import models.Player;
import repositories.GamesRepository;
import repositories.GamesRepositoryJDBC;
import repositories.PlayersRepository;
import repositories.PlayersRepositoryJDBC;

import javax.sql.DataSource;
import java.sql.Connection;
import java.time.LocalDateTime;

public class Main {

    public static void main(String[] args) {

        HikariConfig hikariConfig = new HikariConfig();
        hikariConfig.setJdbcUrl("jdbc:postgresql://localhost:5432/FinalAttestation");
        hikariConfig.setDriverClassName("org.postgresql.Driver");
        hikariConfig.setUsername("postgres");
        hikariConfig.setPassword("1221995");
        hikariConfig.setMaximumPoolSize(20);

        DataSource hikaDataSource = hikariConfig.getDataSource();
        //DataSource dataSource = new DriverManagerDataSource("jdbc:postgresql://localhost:5432/FinalAttestation","postgres","1221995");


        Player firstPlayer = Player.builder().ip("11.221.111.111").name("Альберт").build();
        Player secondPlayer = Player.builder().ip("11.200.11.111").name("Амир").build();

        PlayersRepository playersRepository= new PlayersRepositoryJDBC(hikaDataSource);
        playersRepository.save(firstPlayer);
        playersRepository.save(secondPlayer);


        GamesRepository gamesRepository = new GamesRepositoryJDBC(hikaDataSource);
        Game game = Game.builder().date(LocalDateTime.now()).firstPlayer(firstPlayer).secondPlayer(secondPlayer).gameDuration(0L).build();
        gamesRepository.save(game);
        game.setFirstPlayerShotsCount(20);
        game.setSecondPlayerShotsCount(30);
        gamesRepository.update(game);
        firstPlayer.setWins(1);
        firstPlayer.setLoses(2);
        playersRepository.update(firstPlayer);


    }
}
