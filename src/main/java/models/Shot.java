package models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Shot {
    private Long id;
    private LocalDateTime shotTime;
    private boolean got;
    private Game game;
    private Player shooter;
    private Player target;
}

