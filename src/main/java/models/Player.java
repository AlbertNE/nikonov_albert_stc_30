package models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Objects;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Player {
        private Long id;
        private String ip;
        private String name;
        private int maxPointsCount;
        private int wins;
        private int loses;

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Player user = (Player) o;
            return id.equals(user.id);
        }

        @Override
        public int hashCode() {
            return Objects.hash(ip, name);
        }

}
