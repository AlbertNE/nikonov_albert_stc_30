package models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@Getter
@Builder
public class Game {
    private Long id;
    private LocalDateTime date;
    private Player firstPlayer;
    private Player secondPlayer;
    private Integer firstPlayerShotsCount;
    private Integer secondPlayerShotsCount;
    private Long gameDuration;


}
