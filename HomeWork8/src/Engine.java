public class Engine {
    private Movable[] movables;

    public Engine(Movable[] movables) {
        this.movables = movables;
    }
    public void moveAll(double x, double y)
    {
        for (int i = 0; i < movables.length; i++) {
            movables[i].move(x,y);
        }
    }
}
