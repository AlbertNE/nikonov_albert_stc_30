public  class Figure implements Movable{
    protected Coordinates coordinates;

    public Figure(Coordinates coordinates) {
        this.coordinates = coordinates;
    }

    public Coordinates getCoordinates() {
        return coordinates;
    }

    @Override
    public void move(double x, double y) {
        coordinates.setX(x);
        coordinates.setY(y);
    }
}
