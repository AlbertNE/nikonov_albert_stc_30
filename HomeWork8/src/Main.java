public class Main {

    public static void main(String[] args) {
        Coordinates c1 = new Coordinates(2,4);
        Coordinates c2 = new Coordinates(4,5);

        Rectangle r1 = new Rectangle(c1,5,10);
        System.out.println(r1.getArea());
        System.out.println(r1.getPerimeter());
        Ellipse el1 = new Ellipse(c2,10,10);
        System.out.println(el1.getArea());
        System.out.println(el1.getPerimeter());
        Engine e1 = new Engine(new Movable[]{r1, el1});
        System.out.println(r1.getCoordinates().getX() + " " + r1.getCoordinates().getY());
        System.out.println(el1.getCoordinates().getX() + " " + el1.getCoordinates().getY());
        e1.moveAll(5,5);
        System.out.println(r1.getCoordinates().getX() + " " + r1.getCoordinates().getY());
        System.out.println(el1.getCoordinates().getX() + " " + el1.getCoordinates().getY());

    }
}
