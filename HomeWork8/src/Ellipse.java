public class Ellipse extends Figure implements Scalable{

    double longR;
    double shortR;

    public Ellipse(Coordinates coordinates, double longR, double shortR) {
        super(coordinates);
        this.longR = longR;
        this.shortR = shortR;
    }

    public double getArea() {
        return Math.PI * longR * shortR;
    }
    public double getPerimeter(){
        return 2 * Math.PI * Math.sqrt((longR*longR + shortR*shortR)/2);
    }

    @Override
    public void setTheScale(int scale) {
       this.longR *=scale;
       this.shortR *=scale;
    }
}

