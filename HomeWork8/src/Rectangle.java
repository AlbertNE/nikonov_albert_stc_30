public class Rectangle extends Figure implements Scalable{

    private double width;
    private double length;

    public Rectangle(Coordinates coordinates,double width, double length) {
        super(coordinates);
        this.width = width;
        this.length = length;
    }

    public double getArea() {
        return width * length;
    }

    public double getPerimeter(){
        return 2 * (width + length);
    }

    @Override
    public void setTheScale(int scale) {
        this.width *= scale;
        this.length *= scale;
    }
}
