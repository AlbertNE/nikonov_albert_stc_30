import java.awt.*;
import java.util.function.Function;

public class Car {

    private String carNumber;
    private String model;
    private String color;
    private int mileage;
    private int price;

    public Car(String carNumber, String model, String color, int mileage, int price) {
        this.carNumber = carNumber;
        this.model = model;
        this.color = color;
        this.mileage = mileage;
        this.price = price;
    }

    public String getCarNumber() {
        return carNumber;
    }

    public String getModel() {
        return model;
    }

    public String getColor() {
        return color;
    }

    @Override
    public String toString() {
        return "Car{" +
                "carNumber='" + carNumber + '\'' +
                ", model='" + model + '\'' +
                ", color='" + color + '\'' +
                ", mileage=" + mileage +
                ", price=" + price +
                '}';
    }

    public int getMileage() {
        return mileage;
    }

    public int getPrice() {
        return price;
    }

    public static Function<String, Car> lineToCarFunction = line -> {
        String[] parts = line.split("\\|");

        String carNumber = parts[0];
        String model = parts[1];
        String color = parts[2];
        int mileage = Integer.valueOf(parts[3]);;
        int price = Integer.valueOf(parts[4]);;

        return new Car(carNumber,model,color,mileage,price);
    };

}
