import java.io.*;
import java.util.*;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {


    public static void main(String[] args) {



        ArrayList<Car> cars = new ArrayList<>();
        cars = getCars();
        Predicate<Car> predicate = car -> {
            if (car.getColor() == "Black" || car.getMileage() == 0) {
                return true;
            }
            return false;
        };

        Predicate<Car> predicate2 = car -> {
            if (car.getPrice() >= 700000 && car.getPrice() <= 800000) {
                return true;
            }
            return false;
        };

        Consumer<Car> print = car -> System.out.println(car.toString());
        Consumer<Car> printCarNumber = car -> System.out.println(car.getCarNumber());
        //Function<Car,String> function = car.

        while (true) {
            Stream<Car> carsStream = cars.stream();
            System.out.println("1. Номера всех автомобилей, имеющих черный цвет или нулевой пробег. ");
            System.out.println("2. Количество уникальных моделей в ценовом диапазоне от 700 до 800 тыс. ");
            System.out.println("3. Вывести цвет автомобиля с минимальной стоимостью.");
            System.out.println("4. Средняя стоимость Camry.");

            Scanner scanner = new Scanner(System.in);
            int command = scanner.nextInt();
            scanner.nextLine();

            switch (command) {
                case 1: {
                    carsStream.filter(predicate).forEach(printCarNumber);
                    break;
                }
                case 2: {
                    carsStream.filter(predicate2).
                            map(car -> car.getModel()).
                            distinct().
                            forEach(System.out::println);
                    //cars = getCars();
                    break;
                }
                case 3:{
                    System.out.println(carsStream.min(Comparator.comparingLong(Car::getPrice)).
                           map(car -> car.getColor()).get().toString());
                    break;
                }
                case 4:{

                    System.out.println(carsStream.filter(car -> car.getModel().equals("Camry")).
                            map(car -> car.getPrice()).
                            mapToInt(e -> e).
                            average().getAsDouble());
                    break;
                }
                default:
                    throw new IllegalStateException("Unexpected value: " + command);
            }
        }

    }
    public static ArrayList<Car> getCars()
    {
        ArrayList<Car> cars = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new FileReader("Cars.txt"))) {
            String line = reader.readLine();
            while (line != null) {
                Car fileUser = Car.lineToCarFunction.apply(line);
                cars.add(fileUser);
                line = reader.readLine();
            }
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
        return cars;
    }
}
