import java.util.Arrays;
import java.util.Scanner;

public class NikonovDay3Task1{

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите значения массива через пробел");
        String input = scanner.nextLine();
        String[] arrayString = input.split(" ");
        int[] intArray = new int[arrayString.length];
        for(int i = 0;i < arrayString.length; i++)
        {
            intArray[i] = Integer.valueOf(arrayString[i]);
        }


        System.out.println("Сумма элементов массива:" + elementsSum(intArray));
        reverse(intArray);
        System.out.println("Перевернутый массив:" +Arrays.toString(intArray));
        System.out.println("Среднее арифметическое массива:" + arithmeticMean(intArray));
        changeMaxMin(intArray);
        System.out.println("Массив после замены максимального и минимального эллементов1 :" +Arrays.toString(intArray));
        bubbleSort(intArray);
        System.out.println("Отсортированный массив: " + Arrays.toString(intArray));
        System.out.println("Массив представленный в виде числа: " + arrayToInt(intArray));
    }
    public static int elementsSum(int [] x) {
        int sum = 0;
        for(int i = 0; i < x.length; i++)
        {
            sum += x[i];
        }
        return sum;
    }
    public static void reverse(int [] x) {
        int [] arrayCopy = new int[x.length];

        for(int i = 0; i < x.length; i++)
        {
            arrayCopy[i] = x[i];
        }
        int j = 0;
        for(int i = x.length - 1; i >= 0; i--)
        {
            x[j] = arrayCopy[i];
            j++;
        }
    }
    public static double arithmeticMean(int [] x)
    {
        return elementsSum(x)/(double)x.length;
    }
    public static void changeMaxMin(int [] x) {
        int max = 0;
        int min = 0;
        for(int i = 1; i < x.length; i++)
        {
            if(x[min] > x[i] )
            {
                min = i;
            }
            if(x[max] < x[i] )
            {
                max = i;
            }
        }
        int y = x[min];
        x[min] = x[max];
        x[max] = y;
    }
    public static void bubbleSort(int[] someArray) {
        for(int j = someArray.length ;j >=0;j--) {
            for (int i = 1; i < j; i++) {
                if (someArray[i] < someArray[i - 1]) {
                    int x = someArray[i];
                    someArray[i] = someArray[i - 1];
                    someArray[i - 1] = x;
                }
            }
        }
    }
    public static int arrayToInt(int[] x) {
        int number = 0;
        double degree = 0;
        for(int i = x.length - 1;i >=0;i--)
        {
            number = number + x[i]*(int)Math.pow(10.0,degree);
            degree+=calcIntDex(x[i]);
        }
        return number;
    }
    public static int calcIntDex(int x ) {
        int intDex = 0;
        int f = 1;
        while(f >= 1)
        {
            intDex++;
            f = x/(int)Math.pow(10.0,intDex);
        }
        return intDex;
    }
}
