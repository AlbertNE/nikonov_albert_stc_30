import java.util.Scanner;

public class NikonovDay3Task2 {

    public static double f(double x) {
        return x * x + 2 * x + 5;
    }

    public static double integralSimpson(int a, int b, int n) {
        double sum = 0;
        double h = (b - a) / (double) n;
        for (double k = a + h; k < b; k += 2 * h) {
            sum += f(k - h) + 4 * f(k) + f(k + h);
        }
        return sum * h / 3;
    }

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("f(x) = x*x + 2*x + 5");
        System.out.println("Введите количество шагов(четное):");
        int n = scanner.nextInt();
        System.out.println("Введите начальную точку интервала:");
        int a = scanner.nextInt();
        System.out.println("Введите конечную точку интервала");
        int b = scanner.nextInt();
        String message;
        if (n % 2 == 0) {
            message = "Интеграл функции f(x) на интервале от " + a + " до " + b + " равен " + integralSimpson(a, b, n);
        } else {
            message = "Ошибка: ведено нечетное количество шагов";
        }

        System.out.println(message);

    }
}
