public class Main {

    public static void main(String[] args) {
        Program p1 = new Program("Вести");
        Program p2 = new Program("Вечер с Владимиром Соловьевым");
        Program p3 = new Program("Футбол: Чемпионат Европы");
        Program p4 = new Program("Макорошки всегда одинаково стоят");
        Program p5 = new Program("Они просто сдохнут,а мы сразу в рай");
        Program p6 = new Program("Государство вам ничего не должно");
        Program p7 = new Program("Вести недели");

        Channel channel1 = new Channel("Россия 1");
        channel1.addProgram(p1);
        channel1.addProgram(p2);
        channel1.addProgram(p3);

        Channel channel2 = new Channel("Россия 2");
        channel2.addProgram(p4);
        channel2.addProgram(p5);
        channel2.addProgram(p6);
        channel2.addProgram(p7);

        TV t1 = new TV();
        t1.addChannel(channel1);
        t1.addChannel(channel2);

        RemoteController remoteController = new RemoteController();
        remoteController.setTv(t1);

        System.out.println(remoteController.getChannel(1).getName());
        t1.enableChannelProgram(2);
        System.out.println(channel1.getRandomProgram());

    }
}
