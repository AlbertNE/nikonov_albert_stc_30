import java.util.Random;

public class Channel {
    private String name;
    private final int maxProgramCount = 8;
    private Program[] programs = new Program[maxProgramCount];
    private int count = 0;
    private int number;
    private static int ChannelNumbers = 1;

    public Channel(String name) {
        this.name = name;
        this.number = ChannelNumbers;
        ChannelNumbers++;
    }

    public void addProgram(Program program){
        if(count < maxProgramCount){
            programs[count] = program;
            count++;
        }
    }
    public int getNumber() {
        return number;
    }

    public String getRandomProgram(){
        Random random = new Random(count);
        return programs[count - 1].getName();
    }

    public String getName() {
        return name;
    }


}
