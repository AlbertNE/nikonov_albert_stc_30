public class RemoteController {
    private TV tv;

    public void setTv(TV tv) {
        this.tv = tv;
        tv.setController(this);
    }
    public Channel getChannel(int channelNumber) {
     if(tv != null) {
          return tv.getChannel(channelNumber);
        }
        return null;
    }
}
