public class TV {
    private RemoteController controller;
    private final int maxChannelsCount = 10;
    private int count;
    private Channel[] channels = new Channel[maxChannelsCount];

    public TV() {
        this.count = 0;
    }

    public void setController(RemoteController controller) {
        this.controller = controller;
    }

    public void addChannel(Channel channel)
    {
        if(count < maxChannelsCount) {
            channels[count] = channel;
            count++;
        }
    }

    public void enableChannelProgram(int number){
        Channel channel = getChannel(number);
        if(channel != null) {
            System.out.println(channel.getRandomProgram());
        }
        else{
            System.out.println("Канала с заданным номером не существует");
        }
    }

    public Channel getChannel(int channelNumber) {
       Channel result = null;
       for(int i = 0;i < count;i++){
           if(channels[i].getNumber() == channelNumber){
               result = channels[i];
           }
       }
       return result;
    }

}
