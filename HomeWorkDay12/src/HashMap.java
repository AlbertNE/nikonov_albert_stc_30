public class HashMap<K,V> implements Map<K,V>{
    private static final int MAX_TABLE_SIZE = 8;

    private static class MapEntry<K, V> {
        K key;
        V value;
        MapEntry<K, V> next;

        public MapEntry(K key, V value) {
            this.key = key;
            this.value = value;
        }
    }

    private MapEntry<K, V>[] table;

    public HashMap() {
        this.table = new MapEntry[MAX_TABLE_SIZE];
    }

    @Override
    public void put(K key, V value) {
        // получили хеш ключа, например для "Максим" - 1076425247
        int hash = key.hashCode();
        // обрезали хешкод для диапазона нашей таблицы 1076425247 & 7 = 7
        int index = hash & (table.length - 1);

        // теперь, посмотрим, есть ли в данном индексе уже какие-то элементы, если нет, просто добавляем новый узел
        if (table[index] == null) {
            table[index] = new MapEntry<>(key, value);
        } else {
            // если там уже были элементы
            // нужно проверить, не было ли уже такого ключа? если этот ключ был, то нужно заменить в нем значение
            // начинаем с самого начала
            MapEntry<K,V> current = table[index];

            while (current != null) {
                // если у текущего узла ключ совпал с тем, который мы подали на вход
                // т.е. put("Марсель", 28), а там уже было "Марсель",27 -> нужно заменить 27 на 28
                if (current.key.equals(key)) {
                    // делаем замену значения
                    current.value = value;
                    // останавливаем программу
                    return;
                }
                // обходим связный список
                current = current.next;
            }

            // , просто добавим новый элемент в начало

            // создаем новый элемент
            MapEntry<K, V> newEntry = new MapEntry<>(key, value);
            // указываем, что следующий после нового это тот, который уже был в таблице
            newEntry.next = table[index];
            // теперь новый элемент первый в ячейке
            table[index] = newEntry;
        }
    }

    @Override
    public V get(K key) {

        for (MapEntry current:table) {
            if (current!=null&&current.key.equals(key)) {
                return (V) current.value;
            }
        }
        return null;
    }


}
