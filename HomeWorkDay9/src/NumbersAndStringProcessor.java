public class NumbersAndStringProcessor {
    private int[] intArray;
    private String [] stringArray;

    public NumbersAndStringProcessor(int[] intArray, String[] stringArray) {
        this.intArray = intArray;
        this.stringArray = stringArray;
    }

    public String[] process(StringsProcess process) {
        for (int i = 0; i <stringArray.length ; i++) {
            stringArray[i] = process.process(stringArray[i]);
            System.out.print(stringArray[i] + "  ");
        }
        System.out.println();
        return stringArray;
    }

    public int[] process(NumbersProcess process){
        for (int i = 0; i < intArray.length; i++) {
            intArray[i] = process.process(intArray[i]);
            System.out.print(intArray[i] + "  ");
        }
        System.out.println();
        return intArray;
    }
}
