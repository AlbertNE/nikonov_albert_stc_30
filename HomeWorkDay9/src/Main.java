public class Main {

    public static void main(String[] args) {


        NumbersProcess intReverse = (process) -> {
            String stringInt = String.valueOf(process);
            StringBuilder stringBuilder = new StringBuilder(stringInt);
            stringBuilder.reverse();
            return Integer.valueOf(stringBuilder.toString());
        };

        NumbersProcess removeZeros = (process) ->{
            String stringInt = String.valueOf(process);
            stringInt = stringInt.replaceAll("0","");
            return Integer.valueOf(stringInt);
        };

        NumbersProcess changeTheFuzziness = (process) ->{
            int[] intArray = new int[String.valueOf(process).length()];
            for (int i = 0; i < intArray.length ; i++) {
                int number = (process / (int)Math.pow(10,i ))%10;
                if(number == 1 || number == 0){
                    number = 2;
                }
                else if(number%2 != 0){
                    number -= 1;
                }
                intArray[i] = number;
            }
            int result = 0;
            for (int i = intArray.length - 1; i >= 0; i--) {
                result = result + intArray[i]*(int)Math.pow(10,i);
            }
            return result;
        };


        StringsProcess strReverse = (process) ->{
            StringBuilder stringBuilder = new StringBuilder(process);
            stringBuilder.reverse();
            return stringBuilder.toString();
        };

        StringsProcess removeNumbers = (process) ->{
            return process.replaceAll("\\d","");
        };

        StringsProcess toUpperCase = (process) ->{
            return process.toUpperCase();
        };




        NumbersAndStringProcessor numbersAndStringProcessor = new NumbersAndStringProcessor(new int[]{10001, 243, 11123, 17983}, new String[]{"aa1001", "sdf123112", "aaASDA1414141"});
        numbersAndStringProcessor.process(intReverse);
        numbersAndStringProcessor.process(removeZeros);
        numbersAndStringProcessor.process(changeTheFuzziness);
        numbersAndStringProcessor.process(strReverse);
        numbersAndStringProcessor.process(removeNumbers);
        numbersAndStringProcessor.process(toUpperCase);





    }
}
