package repositories;

import models.User;

import java.util.ArrayList;
import java.util.Optional;
import java.util.stream.Stream;

public class PlayersRepository {
    private ArrayList<User> users;

    public void save(User user)
    {
        boolean inList = users.stream().anyMatch((param) -> param.equals(user));
        if(!inList){
            users.add(user);
        }
    }

    public Optional<User> findByNickname(String nickname){
        nickname = nickname.toLowerCase();
        for (User user:users) {
            if(user.getName().toLowerCase().equals(nickname)) {
                return Optional.of(user);
            }
        }
        return Optional.empty();
    }


}
