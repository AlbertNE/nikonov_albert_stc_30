package models;

import java.util.Date;

public class Shot {
    private Date shotTime;
    private boolean result;
    private Game game;
    private User shooter;
    private User shootable;

    public Shot(Date shotTime, boolean result, Game game, User shooter, User shootable) {
        this.shotTime = shotTime;
        this.result = result;
        this.game = game;
        this.shooter = shooter;
        this.shootable = shootable;
    }

    public Date getShotTime() {
        return shotTime;
    }

    public boolean isResult() {
        return result;
    }

    public Game getGame() {
        return game;
    }

    public User getShooter() {
        return shooter;
    }

    public User getShootable() {
        return shootable;
    }
//    private  static class ShotInfo
//    {
//        private User shooter;
//        private User shootable;
//
//        public User getShooter() {
//            return shooter;
//        }
//
//        public User getShootable() {
//            return shootable;
//        }
//    }


}
