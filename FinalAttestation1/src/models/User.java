package models;

import java.util.Objects;

public class User {
    private String ip;
    private String name;
    private int maxPointsCount;
    private int wins;
    private int loses;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return ip.equals(user.ip) && name.toLowerCase().equals(user.name.toLowerCase());
    }

    @Override
    public int hashCode() {
        return Objects.hash(ip, name);
    }

    public User(String ip, String name) {
        this.ip = ip;
        this.name = name;
        this.wins = 0;
        this.loses = 0;

    }

    public String getIp() {
        return ip;
    }

    public String getName() {
        return name;
    }

    public int getMaxPointsCount() {
        return maxPointsCount;
    }

    public int getWins() {
        return wins;
    }

    public int getLoses() {
        return loses;
    }
}
