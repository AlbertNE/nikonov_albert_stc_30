package models;

import java.util.Date;
import java.util.HashMap;

public class Game {
    private Date date;
    private User [] users;
    HashMap<User,Integer> shots;
    private long gameDuration;

    public Game(Date date, User[] users) {
        this.date = date;
        this.users = users;
    }

    public Date getDate() {
        return date;
    }

    public User[] getUsers() {
        return users;
    }

    public long getGameDuration() {
        return gameDuration;
    }
}
