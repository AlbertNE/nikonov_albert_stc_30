import models.User;
import repositories.UsersRepositoryFileBasedImpl;
import util.IdGeneratorFileBased;

public class Main {

    public static void main(String[] args) {

        IdGeneratorFileBased idGeneratorFileBased = new IdGeneratorFileBased("idList.txt");
        UsersRepositoryFileBasedImpl usersRepository = new UsersRepositoryFileBasedImpl("UserList.txt",idGeneratorFileBased);

        User user = new User("10.312.1231.11","Albert");
        User user2 = new User ("111.2211.112.11","Amir");
        User user3 = new User ("111.2211.112.11","Ramil");
        user3.setId(1);
        usersRepository.save(user);
        usersRepository.save(user2);
        usersRepository.update(user3);
    }
}
