package repositories;

import models.Game;
import models.Shot;

public interface ShotsRepository extends  CrudRepository<Shot>{
}
