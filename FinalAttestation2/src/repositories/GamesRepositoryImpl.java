package repositories;

import models.Game;

import java.util.ArrayList;
import java.util.Optional;

public class GamesRepositoryImpl implements GamesRepository {

    private ArrayList<Game> games;

    @Override
    public void save(Game game) {
        boolean inList = games.stream().anyMatch((param) -> param.equals(game));
        if(!inList){
            games.add(game);
        }
    }

    @Override
    public void update(Game game) {
        for (Game gameItr: games) {
            if(gameItr.equals(game)){
                gameItr = game;
            }
        }
    }

    @Override
    public Optional<Game> findById(int id) {
        for (Game game : games) {
            if(game.getId() == id){
                return Optional.of(game);
            }
        }
        return Optional.empty();
    }
}
