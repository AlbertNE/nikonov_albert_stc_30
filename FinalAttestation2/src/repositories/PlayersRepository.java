package repositories;

import models.Game;
import models.User;

import java.util.Optional;

public interface PlayersRepository extends CrudRepository<User>{
    Optional<User> findByNickname(String nickname);
    void update(User user);
}
