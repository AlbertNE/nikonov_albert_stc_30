package repositories;

import models.User;

import java.util.ArrayList;
import java.util.Optional;

public class PlayersRepositoryImpl implements PlayersRepository {

    private ArrayList<User> users;
    @Override
    public void save(User user) {
        boolean inList = users.stream().anyMatch((param) -> param.equals(user));
        if(!inList){
            users.add(user);
        }
    }

    @Override
    public void update(User user) {
        for (User userItr: users) {
            if(userItr.equals(user)){
                userItr = user;
            }
        }

    }

    @Override
    public Optional<User> findByNickname(String nickname) {
        nickname = nickname.toLowerCase();
        for (User user:users) {
            if(user.getName().toLowerCase().equals(nickname)) {
                return Optional.of(user);
            }
        }
        return Optional.empty();
    }

}
