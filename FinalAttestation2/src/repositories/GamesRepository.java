package repositories;

import models.Game;
import models.User;

import java.util.Optional;

public interface GamesRepository extends CrudRepository<Game> {
    Optional<Game> findById(int id);
    void update(Game game);
}
