package repositories;

import java.util.Optional;

public interface CrudRepository<T> {
    void save(T model);
}
