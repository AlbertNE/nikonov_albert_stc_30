package repositories;

import models.User;
import util.IdGenerator;
import util.IdGeneratorFileBased;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;


public class UsersRepositoryFileBasedImpl implements PlayersRepository {


    private String fileName;
    private IdGeneratorFileBased idGenerator;
    ArrayList<User> users;

    private Function<String, User> lineToUserFunction = line -> {
        String[] parts = line.split("\\|");
        Integer id = Integer.valueOf(parts[0]);
        String ip = parts[1];
        String name = parts[2];
        User user = new User(ip,name);
        user.setId(id);
        return user;
    };

    public UsersRepositoryFileBasedImpl(String fileName, IdGeneratorFileBased idGenerator) {
        this.fileName = fileName;
        this.idGenerator = idGenerator;
        try (Writer writer = new FileWriter(fileName, true)) {
            // проверка наличия файла
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public Optional<User> findByNickname(String nickName) {
        // try with resources - автоматически закрывает поток, если он открыт
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            String line = reader.readLine();
            while (line != null) {
                User user = lineToUserFunction.apply(line);
                if (user.getName().equals(nickName)) {
                    return Optional.of(user);
                }
                line = reader.readLine();
            }
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }

        return Optional.empty();
    }

    @Override
    public void update(User user) {
        ArrayList<User> users = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            String line = reader.readLine();
            while (line != null) {
                User fileUser = lineToUserFunction.apply(line);
                if(user.equals(fileUser)){
                   fileUser = user;
                }
                users.add(fileUser);
                line = reader.readLine();
            }


            //Предварительная очистка файла
            FileWriter fwOb = new FileWriter(fileName, false);
            PrintWriter pwOb = new PrintWriter(fwOb, false);
            pwOb.flush();
            pwOb.close();
            fwOb.close();

            BufferedWriter writer2 = new BufferedWriter(new FileWriter(fileName, true));
            for (User userItr:users) {
                writer2.write(userItr.getId() + "|" + userItr.getIp() + "|" + userItr.getName() + "\n");
            }
            writer2.close();

        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public void save(User model) {
        try {
            model.setId(idGenerator.nextId());
            BufferedWriter writer = new BufferedWriter(new FileWriter(fileName, true));
            writer.write(model.getId() + "|" + model.getIp() + "|" + model.getName() + "\n");
            writer.close();

            BufferedWriter idWriter = new BufferedWriter(new FileWriter(idGenerator.getFileName(), true));
            idWriter.write(model.getId() + "\n");
            idWriter.close();

        } catch (IOException e) {
            throw new IllegalStateException(e);
        }


    }
    public void close(){
        BufferedWriter writer = null;
        try {
            writer = new BufferedWriter(new FileWriter(fileName, true));
            writer.write("");
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
