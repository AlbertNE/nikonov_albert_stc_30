package models;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Objects;

public class Game {

    private static int idNuberСounter = 0;
    private int id;
    private LocalDate date;
    private User [] users;
    HashMap<User,Integer> shots;
    private long gameDuration;

    public Game(LocalDate date, User[] users) {
        this.id = idNuberСounter;
        this.date = date;
        this.users = users;
        idNuberСounter++;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Game game = (Game) o;
        return id == game.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public LocalDate getDate() {
        return date;
    }

    public User[] getUsers() {
        return users;
    }

    public long getGameDuration() {
        return gameDuration;
    }

    public int getId() {
        return id;
    }
}
