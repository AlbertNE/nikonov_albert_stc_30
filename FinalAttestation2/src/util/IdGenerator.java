package util;

public interface IdGenerator {
    Integer nextId();
}
