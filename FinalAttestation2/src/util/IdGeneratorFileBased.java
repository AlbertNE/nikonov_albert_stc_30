package util;

import models.User;

import java.io.*;
import java.util.ArrayList;
import java.util.Optional;


public class IdGeneratorFileBased implements IdGenerator{
    private String fileName;

    public IdGeneratorFileBased(String fileName)  {
        this.fileName = fileName;
        try {
            Writer writer = new FileWriter(fileName, true);
            writer.close();
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public Integer nextId() {
        String lastline = "";
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            String line = reader.readLine();
            while (line != null) {
                lastline = line;
                line = reader.readLine();
            }
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
        if(lastline ==""){
            lastline = "0";
        }
        int id = Integer.valueOf(lastline);
        id++;
        return id;
    }

    public String getFileName() {
        return fileName;
    }
}
